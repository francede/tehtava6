import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class secToTimeTest {
    private int sec;
    private String time;
    
    public secToTimeTest(int sec, String time) {
        this.sec = sec;
        this.time = time;
    }
   
    @Parameters
    public static List testiTapaukset() {
        return Arrays.asList(new Object[][] {
            {0, "0:00:00"},
            {1, "0:00:01"},
            {59, "0:00:59"},
            {60, "0:01:00"},
            {3540, "0:59:00"},
            {3600, "1:00:00"},
            {32400, "9:00:00"},
            {35999, "9:59:59"},
            {-1, "-1"},
            {36000, "-1"},
        });
    }

    @Test
    public void hello() {
         String res = TimeUtils.secToTime(this.sec);
         assertEquals(this.time, res);
    }
}